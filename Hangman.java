import java.util.Scanner;
public class Hangman{

	
	public static int isLetterInWord(String word, char c) {
		//looping through the first 4 letters of the word
	for (int i =0; i<4 ; i++){
		// checking if the letter at i matches letter at c
		if (word.charAt(i)==c){
			//if it does match, returns the position of the letter in the word
			return i;
		}

	}	//if doesnt match, returns -1 to indicate that the letter is not in word
	   return -1;
}
	public static char toUpperCase(char c) {
		//returns letter in uppercase format
   	   return Character.toUpperCase(c);
}

	public static void printWord(String word, boolean [] revealed) {
		//empty string to store result
		String newWord = "";
		//looping through each letter in word
		for (int i = 0; i < 4; i++) {
			//checking if letter at i is revealed
		if (revealed[i]){
			//if revealed add letter to the newWord string
			newWord += word.charAt(i);
		}else{
			// if not revealed, add underscore to represent hidden letter
			newWord += "_";
		}
	}
		//print result
		System.out.println("Your result is "+ newWord);
	}
	
	public static  void runGame(String word){
		//boolean array to track if each letter has been revealed
	boolean[] revealed = new boolean[]{false, false, false, false};
		//counter for the number of guesses
		int numberOfGuess = 0;

		// looping until either max guess reaches or all letters revealed
		while(numberOfGuess < 6 && !(revealed[0] && revealed[1] && revealed[2] && revealed[3])){

			//getting user input
			Scanner reader = new Scanner (System.in);

			System.out.println("Guess a letter");
			char guess = reader.nextLine().charAt(0);
			//converting guess to uppercase
			guess =toUpperCase(guess);

			//checking if guessed letter is in the word
			if(isLetterInWord(word, guess)==0){
				revealed[0]=true;
			}
			if(isLetterInWord(word, guess)==1){
				revealed[1]=true;
			}
			if(isLetterInWord(word, guess)==2){
				revealed[2]=true;
			}
			if(isLetterInWord(word, guess)==3){
				revealed[3]=true;
			}
			//if letter guessed is wrong, adds to number of guesses count
            if(isLetterInWord(word, guess)==-1){
				numberOfGuess++;
			}
			// printing the word and each letter
			printWord(word, revealed);
		}
		//if number of guess reaches 6, print text
		if (numberOfGuess==6){
		System.out.println("Oops! Better luck next time :)" );
		}
		//if all the letters are correct, print text
		if(revealed[0] && revealed[1] && revealed[2] && revealed[3]){
		System.out.println("Congrats! You got it :)");
		}
	}
	public static void main(String[] args){
		Scanner reader = new Scanner (System.in);
		//get user input
		System.out.println("Enter a 4-letter word:");
		String word = reader.next();
		//convert to upper case
		word = word.toUpperCase();

		//start hangman game
	    runGame(word);
	}
}
